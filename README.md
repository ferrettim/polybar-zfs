## polybar-zfs
A small script to output the allocated space and total pool size for a specific ZFS pool.
<img src="polybar-zfs.png" alt="polybar-zfs">

### Usage
1. Copy **polybar-zfs.sh** to **~/.config/polybar/scripts**.
2. Edit polybar-zfs.sh and change **$pool_name** to the name of the pool you want to monitor. If you want to show the ZFS pool health status, also change **$show_health** to **"yes"**.
3. Load the module from polybar.
```[module/polybar-zfs]
type = custom/script
interval = 60
format = <label>
exec = ~/.config/polybar/scripts/polybar-zfs.sh
```

### Notes
- I personally use the MaterialIcons font for my polybar icons, so I recommend that be installed, but any other font with icons should work.
- Feature requests are welcome.
